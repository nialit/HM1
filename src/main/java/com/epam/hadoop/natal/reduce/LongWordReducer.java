package com.epam.hadoop.natal.reduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.ReduceContext;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

public class LongWordReducer extends Reducer<IntWritable, Text, IntWritable, Text> {

    boolean invoked = false;

    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context
    ) throws IOException, InterruptedException {
        if (!invoked) {
            context.write(key, values.iterator().next());
            invoked = true;
        }
    }
}

