package com.epam.hadoop.natal.map;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class LongWordMapperSlow extends Mapper<LongWritable, Text, IntWritable, Text> {

    protected void map(LongWritable key, Text value,
                       Context context) throws IOException, InterruptedException {
        String line = value.toString();
        StringTokenizer tokenizer = new StringTokenizer(line);
        Text maxLengthWord = new Text();
        while (tokenizer.hasMoreTokens()) {
            String curWord = tokenizer.nextToken();
            if (curWord.length() > maxLengthWord.getLength()) {
                maxLengthWord.set(curWord);
                context.write(new IntWritable(maxLengthWord.getLength()), maxLengthWord);
            }
        }
    }

}

