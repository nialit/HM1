package com.epam.hadoop.natal.map;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class LongWordMapper extends Mapper<LongWritable, Text, IntWritable, Text> {

    private Text word = new Text();

    @Override
    public void run(Context context) throws IOException, InterruptedException {
        setup(context);
        try {
            while (context.nextKeyValue()) {
                map(context.getCurrentKey(), context.getCurrentValue(), context);
            }
            context.write(new IntWritable(word.getLength()), word);
        } finally {
            cleanup(context);
        }
    }

    @Override
    protected void map(LongWritable key, Text value,
                       Context context) throws IOException, InterruptedException {
        String line = value.toString();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            String curWord = tokenizer.nextToken();
            if (curWord.length() > word.getLength()) {
                word.set(curWord);
            }
        }
    }

}

