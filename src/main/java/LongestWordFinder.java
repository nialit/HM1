import com.epam.hadoop.natal.map.LongWordMapper;
import com.epam.hadoop.natal.map.LongWordMapperSlow;
import com.epam.hadoop.natal.reduce.LongWordReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class LongestWordFinder {


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        boolean fast = true;
        //make small splits for getting more mappers
        //conf.setLong(
        //   FileInputFormat.SPLIT_MAXSIZE, 1024 * 512);
        Job job = Job.getInstance(conf, "longest word counter");
        job.setJarByClass(LongestWordFinder.class);
        if (args.length > 2 && args[2].equals("slow")) {
            fast = false;
        }
        //don't need combiner, just save the longest word in map and push forward on cleanup
        if (!fast) {
            job.setCombinerClass(LongWordReducer.class);
            job.setMapperClass(LongWordMapperSlow.class);
        } else {
            job.setMapperClass(LongWordMapper.class);
        }
        job.setReducerClass(LongWordReducer.class);
        job.setNumReduceTasks(1);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setSortComparatorClass(DescendingIntWritableComparable.DecreasingComparator.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        long start = System.currentTimeMillis();
        int completed = job.waitForCompletion(true) ? 0 : 1;
        System.out.println("Time:" + String.valueOf(System.currentTimeMillis() - start));
        System.exit(completed);

    }

    public static class DescendingIntWritableComparable extends IntWritable {

        /**
         * A decreasing Comparator optimized for IntWritable.
         */
        public static class DecreasingComparator extends Comparator {

            public int compare(WritableComparable a, WritableComparable b) {
                return -super.compare(a, b);
            }

            public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
                return -super.compare(b1, s1, l1, b2, s2, l2);
            }
        }
    }

}
